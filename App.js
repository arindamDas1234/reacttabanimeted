/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer } from '@react-navigation/native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import Home from './src/home.js';
import Profile from './src/profile.js';
import Cart from './src/cart.js';
import Order from './src/order.js';

import Button from './src/button.js';

const Tab = createBottomTabNavigator();

function CustomButton(){
  return null;
}

export default class App extends Component {
  render(){
     return (
    <NavigationContainer>

  <Tab.Navigator tabBarOptions={{
    showLabel:false
  }} >
  <Tab.Screen name="home" component={Home} options={{
    tabBarIcon: ()=> <Icon name="home-outline" size={20} color="#4a148c" />
  }} />
  <Tab.Screen name="profile" component={Profile}  options={{
    tabBarIcon : ()=> <Icon name="person-outline" size={20} color="#4a148c" />
  }} />
  <Tab.Screen name="CustomButton" component={CustomButton} listeners={{
     tabPress: (e)=> {
      e.preventDefault()
     }
  }} options={{
    tabBarIcon:()=> <Button />
  }} />
  <Tab.Screen name="cart" component={Cart} options={{
    tabBarIcon : ()=> <Icon name="cart-outline" size={20} color="#4a148c" />
  }} />
  <Tab.Screen name="order" component={Order} options={{
    tabBarIcon:()=> <Icon name="document-outline" size={20} color="#4a148c" />
  }} />

  </Tab.Navigator>

    </NavigationContainer>
  );
  }
}

