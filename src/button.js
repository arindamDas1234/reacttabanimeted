import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Animated } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class Button extends Component {

	buttonSize = new Animated.Value(1);
    mode = new Animated.Value(0);

	handlePress = ()=>{
		Animated.sequence([

			Animated.timing(this.buttonSize, {
			toValue:0.95,
			duration:300,
			useNativeDriver:true
		   }),

			Animated.timing(this.buttonSize, {
				toValue:1,
				duration:300,
				useNativeDriver:true
			}),

			Animated.timing(this.mode, {
				toValue: this.mode._value === 0 ? 1: 0,
				useNativeDriver:false
			})
		]).start()
	}

	render(){

		const rotation = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:["0deg", "45deg"]
		});

		const firstButtonX = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:[-24, -100]
		});

		const firstButtonY = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:[-50, -100]
		});

		const seccondButtonX = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:[-24, -24]
		});

		const seccondButtonY = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:[-24, -150]
		});

		const thirdButtonX = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:[-24,50]
		});

		const thirdButtonY = this.mode.interpolate({
			inputRange:[0,1],
			outputRange:[-50, -100]
		})
		return(

			<View style={{
				justifyContent:'center',
				alignItems:'center'
			}} >
	<Animated.View style={{ positoin:'absolute', left:firstButtonX, top:firstButtonY }} >

	<View style={{
		position:'absolute',
		justifyContent:'center',
		alignItems:'center',
		height:40,
		width:40,
		borderRadius:20,
		backgroundColor:"#4a148c"
	}} >
 <Icon name="airplane-outline" size={16} color="#FFF" />
	</View>

	</Animated.View>

	<Animated.View style={{ position:"absolute", left:seccondButtonX, top:seccondButtonY  }} >
		<View style={{
			position:"absolute",
			justifyContent:'center',
			alignItems:'center',
			height:40,
			width:40,
			borderRadius:40/2,
			backgroundColor:"#4a148c"
		}} >
	<Icon name="cart-outline" color="#FFF" size={16} />

		</View>
	</Animated.View>

 <Animated.View style={{ position:"absolute", left:thirdButtonX, top:thirdButtonY }} >

 	<View style={{
 		position:"absolute",
 		justifyContent:'center',
 		alignItems:'center',
 		height:40,
 		width:40,
 		borderRadius:20,
 		backgroundColor:"#4a148c"
 	}} >
  <Icon name="bicycle-outline" color="#FFF" size={20} />

 	</View>
 </Animated.View>

		<View style={{
			position:'absolute',
			top:-60,
			alignItems:'center',
			justifyContent:"center",
			height:76,
			width:76,
			borderRadius:76 /2,
			backgroundColor:"#4a148c",
			shadowColor:'#333',
			shadowOffset: {
				height: 30
			},
			shadowOpacity: 0.3
		}} >
	<TouchableOpacity onPress={()=> this.handlePress() } >
<Animated.View style={{ transform:[{scale: this.buttonSize }] }} >

 <Animated.View style={{ transform:[{rotate:rotation}] }} >
<Icon name="add" color="#FFF" size={45}  />
 </Animated.View>

	</Animated.View>
	</TouchableOpacity>

		</View>
			</View>
		)
	}
}